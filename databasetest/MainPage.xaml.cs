﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Data.SQLite;
using System.Data;
// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace databasetest
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }
       // public SQLiteConnection sql_conn;
        //public SQLiteCommand sql_cmd;
        
        //string connectionstring= @"D:\study\5th semester\Visual Programming\databasetest\databasetest\bin\x86\Debug\connectiondb.db;Version=3;";
        
        private void setconnection()
        {
            

        } 
        private async void Signin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var conn = new SQLiteConnection(@"Data Source=D:\study\5th semester\Visual Programming\databasetest\databasetest\bin\x86\Debug\testdb.db;Version=3"))
                {
                    conn.Open();
                    using (var cmd = new SQLiteCommand("SELECT username,password FROM userlogin WHERE username='@username' AND password = '@password'", conn))
                    {
                        cmd.Parameters.AddWithValue("@username", username.Text);
                        cmd.Parameters.AddWithValue("@password", userpass.Password);
                        using (var reader = cmd.ExecuteReader())
                        {
                            var count = 0;
                            while (reader.Read())
                            {
                                count = count + 1;
                            }
                            if (count == 1)
                            {
                                string message = "correct credentials";
                                MessageDialog msgdialog = new MessageDialog(message, "Alert!");
                                await msgdialog.ShowAsync();
                                this.Frame.Navigate(typeof(newpage));
                            }
                            else if (count == 0)
                            {
                                string message = "Error in credentials";
                                MessageDialog msgdialog = new MessageDialog(message, "Alert!");
                                await msgdialog.ShowAsync();

                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                
            }
        }
    }
    }

